from typing import Any
class CURRENT_APPROACH_NAME :
    NAME = "OURS"

class ExistingResult:
    """
    Class to store the known results of previous approaches.
    method getExistingResultsForMetric can be used in any metric script
    """

    existingResults = None

    @classmethod
    def getExistingResultsForMetric(cls,name: str) -> Any:
        """
        Get the existing results for a given metric, load in the file, which path is given in protocole
        Return None if no results are available
        Format of the specified file :
        BEGIN of file
        {
            "MetricName1(as specified in _init_ of metrics)" :
                    [{"name":"nameOfTheApproach",
                    "abciss": range(-40,1), # pythoninc way or list like below, length should match with below
                    "results": [0.14,0.15,0.15,0.15,0.16,0.17,0.18,0.19,0.20,0.20,0.21,0.22,0.23,0.23,0.25,0.26,0.28,0.31,0.35,0.36,0.37,0.40,0.40,0.43,0.44,0.44,0.45,0.47,0.48,0.49,0.50,0.50,0.51,0.54,0.55,0.56,0.56,0.57,0.57,0.57,0.58],
                    "color" : "blue" # optional, if not specified, random color is used
                    }]
            }
        END of file

        Can be used with summary method of Metric class to plot the results, adding "_summary" at the end of the metric name
        Example of file is available in "exampleExistingResults.txt".


        Note that the content of file will be directly been passed in the python eval() method,
        so be careful that the python syntax is correct
        :param name:
        :return: the specific previous results for the given metric
        """
        if (cls.existingResults is not None) and (name in cls.existingResults):
            return cls.existingResults[name]
        return None