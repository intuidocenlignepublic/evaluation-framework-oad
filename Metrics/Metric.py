from __future__ import annotations  # needs >=python 3.7, otherwise remove typing of Metric

from abc import abstractmethod
from typing import List, Tuple, Dict

import numpy as np
from matplotlib import pyplot as plt

from Metrics.ExistingResult import ExistingResult, CURRENT_APPROACH_NAME


class Metric():

    def __init__(self, name):
        self.name = name
        self.resultsToSummarize: Dict[str, Tuple[float, float]] = {}
        """
        resultsToSummarize is a  dict of the form
                {name: (indexOfVal, value)}
        """

    @staticmethod
    def Summary(results: List[Metric], pathOutputResultGeneral: str):
        """
        Print the summary of the results for differents instances of the same metric

        :param results_with_differents_alpha:
        :return:
        """
        # print("Summary of the results for the metric " + results[0].name)
        # print("Summary resultsToSummarize.keys() " + str(results[0].resultsToSummarize.keys()))
        keys = results[0].resultsToSummarize.keys()
        # print(results)
        for key in keys:
            resIndex: List[float] = []
            resVals: List[float] = []
            for result in results:
                resIndex.append(result.resultsToSummarize[key][0])
                val = result.resultsToSummarize[key][1]
                val = (0 if val is None or np.isnan(val) else val)
                resVals.append(val)



            plt.plot(resIndex, resVals, color="red")
            keySplit = key.split("_")
            plt.xlabel(keySplit[0])
            plt.ylabel(keySplit[1])
            plt.legend(["Ours"])
            plt.grid(linewidth=0.25)
            pathOut = pathOutputResultGeneral + "/" + results[0].name + "_summary_" + key
            plt.savefig(pathOut + ".png", bbox_inches='tight')

            with open(pathOut + ".txt", "w") as f:
                f.write(",".join(["%.3f" % i for i in resIndex]))
                f.write("\n")
                f.write(",".join([ "%.3f" % i for i in resVals]))

            # plt.savefig(pathOutput[:-4], bbox_inches='tight', format='eps')
            legend = [CURRENT_APPROACH_NAME.NAME]
            existingResults = ExistingResult.getExistingResultsForMetric(results[0].name + "_summary")
            if existingResults is not None:
                existingResults = existingResults[key]
                try:
                    for existingResult in existingResults:
                        legend.append(existingResult["name"])
                        if "color" in existingResult:
                            plt.plot(existingResult["abciss"], existingResult["results"], color=existingResult["color"])
                        else:
                            plt.plot(existingResult["abciss"], existingResult["results"])
                except TypeError:
                    print(
                        "The furnished existing results are not iterable, give a list of existing results for metric ",
                        results[0].name + "_summary")
                plt.legend(legend)
                plt.savefig(pathOut + "_comparison.png", bbox_inches='tight')

            plt.close()

    @abstractmethod
    def AggregateMultiFold(self, resultsFolds: List, pathResult: str, filesInFold:List[str]):
        pass
