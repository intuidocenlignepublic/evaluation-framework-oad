import os
from typing import List

import numpy as np
from matplotlib import pyplot as plt
from sklearn.metrics import accuracy_score, average_precision_score, balanced_accuracy_score, \
    confusion_matrix, precision_score, recall_score, f1_score, roc_auc_score, classification_report, \
    ConfusionMatrixDisplay

from Classes.Label import Label
from Classes.Sequence import Sequence
from Metrics.FrameBasedMetrics.FrameBasedMetric import FrameBasedMetric


class PerFrame_All(FrameBasedMetric):
    def __init__(self, nbClass, exportPerSequenceDetailResult):
        super().__init__("EvalPerFrame")
        self.nbClass = nbClass
        self.exportPerSequenceDetailResult = exportPerSequenceDetailResult

    def Evaluate(self, sequence: List[Sequence], prediction: List[List[int]], pathExport):

        if self.exportPerSequenceDetailResult:
            for i in range(len(sequence)):
                try:
                    acc, ap, balanced_acc, acc_onlyClass, confMat, precision, recall, \
                    f1, roc_auc, precision_micro, recall_micro, f1_micro, \
                    roc_auc_micro, precision_perClass, recall_perClass, f1_perClass, \
                    roc_auc_perClass = self.EvaluateSequence(sequence[i].getPerFrameLabel(), prediction[i])

                    pathSeq = pathExport + "Sequence/" + sequence[i].name + "/"
                    if not os.path.exists(pathSeq):
                        os.makedirs(pathSeq)
                    pathSeq+= self.name+".txt"
                    self.exportPerf(pathSeq, acc, ap, balanced_acc, acc_onlyClass, confMat, precision, recall,
                                    f1, roc_auc, precision_micro, recall_micro, f1_micro, roc_auc_micro, precision_perClass,
                                    recall_perClass, f1_perClass, roc_auc_perClass)
                except ShorterPredictionException as e:
                    print("Error in sequence ", sequence[i].name)
                    print(str(e))
        flattedPred = []
        flattedTrue = []
        for i in range(len(sequence)):
            flattedPred += prediction[i]
            ft = sequence[i].getPerFrameLabel()
            if len(prediction[i]) < len(ft):
                if len(ft) - len(prediction[i]) > 2:
                    print("Warning: prediction of "+sequence[i].name+" is shorter than the sequence length of ",len(ft)-len(prediction[i]))
                ft = ft[:len(prediction[i])]

            flattedTrue += list(np.pad(ft, (0, len(prediction[i]) - len(ft)), 'constant',constant_values=0))
        assert len(flattedPred) == len(flattedTrue)
        acc, ap, balanced_acc, acc_onlyClass, confMat, precision, recall, \
        f1, roc_auc, precision_micro, recall_micro, f1_micro, \
        roc_auc_micro, precision_perClass, recall_perClass, f1_perClass, \
        roc_auc_perClass = self.EvaluateSequence(flattedTrue, flattedPred)

        if not os.path.exists(pathExport) :
            os.makedirs(pathExport)
        self.exportPerf(pathExport+self.name+".txt", acc, ap, balanced_acc, acc_onlyClass, confMat, precision, recall,
                        f1, roc_auc, precision_micro, recall_micro, f1_micro, roc_auc_micro, precision_perClass,
                        recall_perClass, f1_perClass, roc_auc_perClass)
        return acc,balanced_acc,acc_onlyClass,precision,recall,f1,precision_micro,recall_micro,f1_micro,precision_perClass,recall_perClass,f1_perClass

    def AggregateMultiFold(self, outputResultMetric, pathOutputResultGeneral, filesFold):
        acc = []
        balanced_acc = []
        acc_onlyClass = []
        precision = []
        recall = []
        f1 = []
        precision_micro = []
        recall_micro = []
        f1_micro = []
        precision_perClass = []
        recall_perClass = []
        f1_perClass = []
        for res in outputResultMetric:
            acc_res,balanced_acc_res,acc_onlyClass_res,precision_res,recall_res,f1_res,precision_micro_res,\
                recall_micro_res, f1_micro_res,precision_perClass_res,recall_perClass_res,f1_perClass_res = res
            acc.append(acc_res)
            balanced_acc.append(balanced_acc_res)
            acc_onlyClass.append(acc_onlyClass_res)
            precision.append(precision_res)
            recall.append(recall_res)
            f1.append(f1_res)
            precision_micro.append(precision_micro_res)
            recall_micro.append(recall_micro_res)
            f1_micro.append(f1_micro_res)
            precision_perClass.append(precision_perClass_res)
            recall_perClass.append(recall_perClass_res)
            f1_perClass.append(f1_perClass_res)

        acc_avg = np.mean(acc)
        # balanced_acc_avg = np.mean(balanced_acc)
        acc_onlyClass_avg = np.mean(acc_onlyClass)
        precision_avg = np.mean(precision)
        recall_avg = np.mean(recall)
        f1_avg = np.mean(f1)
        precision_micro_avg = np.mean(precision_micro)
        recall_micro_avg = np.mean(recall_micro)
        f1_micro_avg = np.mean(f1_micro)
        precision_perClass_avg = np.mean(precision_perClass, axis=0)
        recall_perClass_avg = np.mean(recall_perClass, axis=0)
        f1_perClass_avg = np.mean(f1_perClass, axis=0)
        self.exportPerf(pathOutputResultGeneral+self.name+".txt", acc_avg, None, None, acc_onlyClass_avg, None,
                        precision_avg, recall_avg, f1_avg, None, precision_micro_avg, recall_micro_avg,
                        f1_micro_avg, None, precision_perClass_avg, recall_perClass_avg,
                        f1_perClass_avg, None)

    def exportPerf(self, pathExport, acc, ap, balanced_acc, acc_onlyClass, confMat, precision,
                   recall, f1, roc_auc, precision_micro, recall_micro, f1_micro, roc_auc_micro,
                   precision_perClass, recall_perClass, f1_perClass, roc_auc_perClass):

        evalstr = ""
        if confMat is not None:
            FP = confMat.sum(axis=0) - np.diag(confMat)
            FN = confMat.sum(axis=1) - np.diag(confMat)
            TP = np.diag(confMat)
            count = sum(confMat.flat)
            countLine = confMat.sum(axis=0)
            assert confMat.shape[0] == self.nbClass+1 and confMat.shape[1] == self.nbClass+1
            fig, ax = plt.subplots(figsize=(15, 15))
            disp = ConfusionMatrixDisplay(confMat, display_labels=[i for i in range(self.nbClass+1)])
            disp.plot(cmap='Blues',ax=  ax)
            plt.savefig(pathExport[:-4] + "_ConfusionMatrix.png")
            plt.close()

            evalstr += "TP" + "\t" + str(" ; ".join(map(str,TP))) + "\n"
            evalstr += "FP" + "\t" + str(" ; ".join(map(str,FP))) + "\n"
            evalstr += "FN" + "\t" + str(" ; ".join(map(str,FN))) + "\n"
            evalstr += "CountL" + "\t" +  str(" ; ".join(map(str,countLine))) + "\n"
            evalstr += "Count" + "\t" + str(count) + "\n"
        evalstr += "Accuracy: " + str(acc) + "\n"
        # evalstr += "Average Precision: " + str(ap) + "\n" # not possible without the score (confidence)
        # evalstr += "Balanced Accuracy: " + str(balanced_acc) + "\n" = Macro average of Recall
        evalstr += "Accuracy Only Classes: " + str(acc_onlyClass) + "\n"
        evalstr += "Precision: " + str(precision) + "\n"
        evalstr += "Recall: " + str(recall) + "\n"
        evalstr += "F1: " + str(f1) + "\n"
        evalstr += "ROC AUC: " + str(roc_auc) + "\n"
        evalstr += "Precision Micro: " + str(precision_micro) + "\n"
        evalstr += "Recall Micro: " + str(recall_micro) + "\n"
        evalstr += "F1 Micro: " + str(f1_micro) + "\n"
        # evalstr += "ROC AUC Micro: " + str(roc_auc_micro) + "\n"

        evalstr += "Precision Per Class: \n" +  str(" ; ".join(map(lambda x: "%.3f" % x,precision_perClass)))  + "\n"
        evalstr += "Recall Per Class: \n" + str(" ; ".join(map(lambda x: "%.3f" % x,recall_perClass))) + "\n"
        evalstr += "F1 Per Class: \n" + str(" ; ".join(map(lambda x: "%.3f" % x,f1_perClass))) + "\n"
        # evalstr += "ROC AUC Per Class: \n" + str(roc_auc_perClass) + "\n"

        f = open(pathExport, "w+")
        f.write(evalstr)
        f.close()

        return evalstr



    def EvaluateSequence(self, sequence: List[int], prediction: List[int]):
        perFrameLabel = np.array(sequence) #.getPerFrameLabel()
        prediction = np.array(prediction)
        if len(prediction) - len(perFrameLabel)<-10:
            print("Error: prediction is shorter than ground truth ",
                  len(prediction), len(perFrameLabel),"->",len(prediction)-len(perFrameLabel), "frames missing. Skip this sequence")
            raise ShorterPredictionException("Prediction is shorter than ground truth")

        if len(prediction) > len(perFrameLabel):
            perFrameLabel = np.pad(perFrameLabel, (0,  len(prediction)-len(perFrameLabel)), 'constant',
                               constant_values=0)
        else:
            perFrameLabel = perFrameLabel[:len(prediction)]

        assert len(perFrameLabel) == len(prediction)

        acc = accuracy_score(perFrameLabel, prediction)
        ap = None #average_precision_score(perFrameLabel, prediction) cant compute without confidence score
        balanced_acc = None # balanced_accuracy_score(perFrameLabel, prediction)
        acc_onlyClass = accuracy_score(perFrameLabel, prediction, sample_weight=(perFrameLabel != 0).astype(float))
        confMat = confusion_matrix(perFrameLabel, prediction,labels=[i for i in range(self.nbClass+1)])

        # macro
        precision = precision_score(perFrameLabel, prediction, average="macro",zero_division=0)
        recall = recall_score(perFrameLabel, prediction, average="macro",zero_division=0)
        f1 = f1_score(perFrameLabel, prediction, average="macro",zero_division=0)
        roc_auc = None #roc_auc_score(perFrameLabel, prediction) cant compute without confidence score

        # micro
        precision_micro = precision_score(perFrameLabel, prediction, average="micro",zero_division=0)
        recall_micro = recall_score(perFrameLabel, prediction, average="micro",zero_division=0)
        f1_micro = f1_score(perFrameLabel, prediction, average="micro",zero_division=0)
        roc_auc_micro = None #roc_auc_score(perFrameLabel, prediction, average="micro") cant compute without confidence score

        # per class
        precision_perClass = precision_score(perFrameLabel, prediction, average=None,zero_division=0)
        recall_perClass = recall_score(perFrameLabel, prediction, average=None,zero_division=0)
        f1_perClass = f1_score(perFrameLabel, prediction, average=None,zero_division=0)
        roc_auc_perClass = None #roc_auc_score(perFrameLabel, prediction, average=None) cant compute without confidence score

        return acc, ap, balanced_acc, acc_onlyClass, confMat, \
               precision, recall, f1, roc_auc, \
               precision_micro, recall_micro, f1_micro, roc_auc_micro, \
               precision_perClass, recall_perClass, f1_perClass, roc_auc_perClass

class ShorterPredictionException(Exception):
    def __init__(self,msg):
        super(ShorterPredictionException, self).__init__(msg)
