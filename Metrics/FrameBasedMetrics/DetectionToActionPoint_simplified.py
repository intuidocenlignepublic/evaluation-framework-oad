import os
from typing import List

import numpy as np
from matplotlib import pyplot as plt

from Classes.Sequence import Sequence
from Metrics.FrameBasedMetrics.FrameBasedMetric import FrameBasedMetric


class DetectionToActionPoint_simplified(FrameBasedMetric):
    """
    CF Bloom metric,
    if these is no action point, then action end is used
    """
    def __init__(self, nbClass, exportPerSequenceDetailResult):
        super().__init__("DetectionToActionPoint_simplified")
        self.nbClass = nbClass
        self.exportPerSequenceDetailResult = exportPerSequenceDetailResult
        self.nbFrameBefore = 40
        self.nbFrameAfter = 10

    def Evaluate(self, sequence: List[Sequence], prediction: List[List[int]], pathExport):
        GoodPredictions = np.zeros([self.nbFrameBefore + 1 + self.nbFrameAfter])  # 20 trames before,0included
        PredictionsDone = np.zeros([
                                       self.nbFrameBefore + 1 + self.nbFrameAfter])  # not necessary the same for each frame (eg. end of sequences)
        for i, seq in enumerate(sequence):
            goodPred, predDone = self.EvaluateSequence(seq, prediction[i])
            GoodPredictions += goodPred
            PredictionsDone += predDone
            if self.exportPerSequenceDetailResult:
                pathSeq = pathExport + "/Sequence/" + sequence[i].name + "/"
                if not os.path.exists(pathSeq):
                    os.makedirs(pathSeq)
                self.ExportPerformance(goodPred, predDone, pathSeq + "/" + self.name + ".txt",exportGraphic=False)

        self.ExportPerformance(GoodPredictions, PredictionsDone,
                               pathExport + "/" + self.name + ".txt")

    def EvaluateSequence(self, sequence: Sequence, prediction: List[int]):
        """

        :param results: list of [filename, predictions[nbClass], reject, GroundTruth]
        :return:
        """
        GoodPredictions = np.zeros([self.nbFrameBefore + 1 + self.nbFrameAfter])  # 20 trames before,0included
        PredictionsDone = np.zeros(
            [self.nbFrameBefore + 1 + self.nbFrameAfter])  # we do not predict something at each temporal frame
        # because of CuDi
        for lab in sequence.labels:
            actStart, actPoint, actEnd = lab.startFrame, lab.actionPoint, lab.endFrame
            if actPoint is None :
                actPoint = actEnd
            classLabel = lab.classID
            framesConcerned = range(max(0, actPoint - self.nbFrameBefore),
                                    min(actPoint + 1 + self.nbFrameAfter,len(prediction)-1))  # temporal frames
            for id, frame in enumerate(framesConcerned):
                id = self.nbFrameBefore - (
                        actPoint - frame)  # recomputed id, because the max operation can cause toubles
                if frame >= actStart and frame <= actEnd:
                    PredictionsDone[id] += 1
                    if prediction[frame] == classLabel:
                        GoodPredictions[id] += 1

        return GoodPredictions, PredictionsDone

    def ExportPerformance(self, GoodPredictions, PredictionsDone, pathOutput,exportGraphic=True):
        res = ""
        res += "accuracyPerFrame,\n" + str(
            " ; ".join(list(["{:.2f}".format(v) for v in np.divide(GoodPredictions, PredictionsDone,
                                                                   out=np.zeros_like(GoodPredictions),
                                                                   where=PredictionsDone != 0)]))) + "\n"
        res += "GoodPredictions,\n" + str(
            " ; ".join(list(["{:.2f}".format(v) for v in GoodPredictions]))) + "\n"
        res += "PredictionsDone,\n" + str(
            " ; ".join(list(["{:.2f}".format(v) for v in PredictionsDone]))) + "\n"
        f = open(pathOutput, "w+")
        f.write(res)
        f.close()
        if exportGraphic:  # plot lines per seq
            absicsMoi = list(range(-self.nbFrameBefore, self.nbFrameAfter + 1))
            plt.plot(absicsMoi, np.divide(GoodPredictions, PredictionsDone,
                                                                   out=np.zeros_like(GoodPredictions),
                                                                   where=PredictionsDone != 0), color="red")
            plt.xlabel("Distance to action point")
            plt.ylabel("Accuracy")
            plt.grid(linewidth=0.25)
            plt.savefig(pathOutput + ".png", bbox_inches='tight')
            plt.close()


