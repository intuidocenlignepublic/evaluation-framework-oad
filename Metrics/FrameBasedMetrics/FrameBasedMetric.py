from abc import abstractmethod
from typing import List

from Classes.Label import Label
from Classes.Sequence import Sequence
from Metrics.Metric import Metric


class FrameBasedMetric(Metric):
    def __init__(self, name):
        super().__init__(name)
        self.name = name

    @abstractmethod
    def Evaluate(self, sequence: List[Sequence], prediction: List[List[int]],pathExport):
        pass

    @abstractmethod
    def EvaluateSequence(self, sequence: Sequence, prediction: List[int]):
        pass
