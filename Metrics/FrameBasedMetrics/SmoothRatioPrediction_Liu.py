import os
from typing import List

import numpy as np
from matplotlib import pyplot as plt

from Classes.Sequence import Sequence
from Metrics.FrameBasedMetrics.FrameBasedMetric import FrameBasedMetric


class SmoothRatioObservation_Liu(FrameBasedMetric):
    def __init__(self, nbClass, exportPerSequenceDetailResult):
        super().__init__("SmoothRatioPrediction_Liu")
        self.nbClass = nbClass
        nbSplitObs = 21
        self.predictAt = np.linspace(0, 100, nbSplitObs)  # 0,5,10, ... 95, 100
        self.exportPerSequenceDetailResult = exportPerSequenceDetailResult


    def Evaluate(self, sequence: List[Sequence], prediction: List[List[int]], pathExport):

        ScorePredictionRatio = np.zeros([len(self.predictAt)])
        TotalPredictionForRatio = np.zeros([len(self.predictAt)])
        for ids, seq in enumerate(sequence):
            ScorePredictionRatio_seq, TotalPredictionForRatio_seq = self.EvaluateSequence(seq, prediction[ids])
            if self.exportPerSequenceDetailResult:
                pathSeq = pathExport + "/Sequence/" + sequence[ids].name + "/"
                if not os.path.exists(pathSeq):
                    os.makedirs(pathSeq)

                self.ExportPerformance(ScorePredictionRatio_seq, TotalPredictionForRatio_seq,
                                       pathSeq + self.name  + ".txt")
            ScorePredictionRatio += ScorePredictionRatio_seq
            TotalPredictionForRatio += TotalPredictionForRatio_seq

        self.ExportPerformance(ScorePredictionRatio, TotalPredictionForRatio, pathExport + self.name + ".txt")
        return ScorePredictionRatio/TotalPredictionForRatio

    def EvaluateSequence(self, sequence: Sequence, prediction: List[int]):


        ScorePredictionRatio = np.zeros([len(self.predictAt)])
        TotalPredictionForRatio = np.zeros([len(self.predictAt)])
        sequenceFramesGT = np.array(sequence.getPerFrameLabel())
        prediction = np.array(prediction)
        for lab in sequence.labels:
            numberOfFrameForThisAction = lab.endFrame - lab.startFrame  # scalar
            framesCorrepondingToRatioObs = self.predictAt * numberOfFrameForThisAction / 100  # vector containing nbSplitObs values between 0 and numberOfFrameForThisAction
            realFrameIndexes = lab.startFrame + framesCorrepondingToRatioObs  # vector containing nbSplitObs values between actStart and actEnd
            realFrameIndexes = realFrameIndexes.round().astype(np.int)
            for idObsRatio, endFractionObsFrame in enumerate(realFrameIndexes):
                TotalPredictionForRatio[idObsRatio] += 1
                correctClassif = np.sum(prediction[lab.startFrame:endFractionObsFrame+1] == sequenceFramesGT[lab.startFrame:endFractionObsFrame+1])
                ScorePredictionRatio[idObsRatio] += correctClassif / (endFractionObsFrame-lab.startFrame+1)

        return ScorePredictionRatio, TotalPredictionForRatio

    def ExportPerformance(self, ScorePredictionRatio, TotalPredictionForRatio, pathOutput):
        res = ""
        res += "accuracyPredictionPerFrame,\n" + str(
            " ; ".join(list(["{:.2f}".format(v) for v in ScorePredictionRatio/TotalPredictionForRatio]))) + "\n"
        res += "ScorePredictionRatio,\n" + str(
            " ; ".join(list(["{:.2f}".format(v) for v in ScorePredictionRatio]))) + "\n"
        res += "PredictionsDonePerRatio,\n" + str(
            " ; ".join(list(["{:.2f}".format(v) for v in TotalPredictionForRatio]))) + "\n"
        # print(res)
        f = open(pathOutput, "w+")
        f.write(res)
        f.close()

        plt.plot(self.predictAt, ScorePredictionRatio/TotalPredictionForRatio, color = "red")
        plt.xlabel("Observation Ratio")
        plt.ylabel("Accuracy")
        plt.savefig(pathOutput[:-4]+".png", bbox_inches='tight')
        plt.close()

    def AggregateMultiFold(self, outputResultMetric: List, pathOutputResultGeneral, filesFold):
        result = np.average(outputResultMetric, axis=0)

        res = ""
        res += "accuracyPredictionPerFrame,\n" + str(
            " ; ".join(list(["{:.2f}".format(v) for v in result]))) + "\n"
        # print(res)
        f = open(pathOutputResultGeneral +self.name+ ".txt", "w+")
        f.write(res)
        f.close()

        plt.plot(self.predictAt, result, color="red")
        plt.xlabel("Observation Ratio")
        plt.ylabel("Accuracy")
        plt.savefig(pathOutputResultGeneral +self.name+".png", bbox_inches='tight')
        plt.savefig(pathOutputResultGeneral +self.name+".eps", bbox_inches='tight', format='eps')
        plt.close()