import os
from typing import List

import numpy as np
from matplotlib import pyplot as plt
from sklearn.metrics import ConfusionMatrixDisplay

from Classes.Label import Label
from Classes.Sequence import Sequence

from Metrics.BoundsBasedMetrics.BoundsBasedMetric import BoundsBasedMetric


class LatencyAwareScore(BoundsBasedMetric):
    """
    This is the metric used in the paper of Fothergill
    "Instructing people for training gestural interactive systems" (MSRC12, 2012)
    If no action point is found, use the end frame of the gesture instead
    """

    def __init__(self, nbClass: int, exportPerSequenceDetailResult: bool, deltaFrames: int,
                 useStartFrameInsteadOfNegativeDelta: bool):
        """

        :param nbClass:
        :param exportPerSequenceDetailResult:
        :param deltaFrames: the number of frames around the action point where a detection will be considered as positive
                            [ActionPoint-deltaFrames, ActionPoint+deltaFrames]
        :param useStartFrameInsteadOfNegativeDelta: if true, the detection will be considered as positive if it is in the interval
                                                    [Start-deltaFrames, ActionPoint+deltaFrames]
        """
        super().__init__("LatencyAwareScore")
        self.nbClass = nbClass
        self.exportPerSequenceDetailResult = exportPerSequenceDetailResult
        self.deltaFrames = deltaFrames
        self.useStartFrameInsteadOfNegativeDelta = useStartFrameInsteadOfNegativeDelta

    def Evaluate(self, sequence: List[Sequence], prediction: List[List[Label]], pathExport: str):
        assert len(sequence) == len(prediction)
        total_TP_per_class = np.zeros([self.nbClass + 1])
        total_FP_per_class = np.zeros([self.nbClass + 1])
        total_Count_per_class = np.zeros([self.nbClass + 1])
        total_matconf = np.zeros([self.nbClass + 1, self.nbClass + 1])
        strUseStartFrameToEvaluate = "useStartFrame" if self.useStartFrameInsteadOfNegativeDelta else "useActionPointLessDelta"
        if not os.path.exists(pathExport):
            os.makedirs(pathExport)
        for i in range(len(sequence)):
            TP_per_class, FP_per_class, Count_per_class, Mat_conf = self.EvaluateSequence(sequence[i],
                                                                                          prediction[i])

            if self.exportPerSequenceDetailResult:
                pathSeq = pathExport + "/Sequence/" + sequence[i].name + "/"
                if not os.path.exists(pathSeq):
                    os.makedirs(pathSeq)

                self.ExportPerformance(TP_per_class, FP_per_class, Count_per_class, Mat_conf,
                                       pathSeq + self.name + "_" + strUseStartFrameToEvaluate + "_" + str(
                                           self.deltaFrames) + ".txt")

            total_TP_per_class += TP_per_class
            total_FP_per_class += FP_per_class
            total_Count_per_class += Count_per_class
            total_matconf += Mat_conf

        self.ExportPerformance(total_TP_per_class, total_FP_per_class, total_Count_per_class, total_matconf,
                               pathExport + "/" + self.name + "_" + strUseStartFrameToEvaluate + "_" + str(
                                   self.deltaFrames) + ".txt")
        # precision_per_class = total_TP_per_class / (total_TP_per_class + total_FP_per_class)
        # recall_per_class = total_TP_per_class / (total_TP_per_class + total_Count_per_class)
        # F1_per_class = 2 * precision_per_class * recall_per_class / (precision_per_class + recall_per_class)
        return total_TP_per_class, total_FP_per_class, total_Count_per_class

    def EvaluateSequence(self, sequence: Sequence, prediction: List[Label]):
        nbClass = self.nbClass
        flags = np.zeros([len(sequence.labels)])
        sequence.labels = sorted(sequence.labels, key=lambda x: x.startFrame)
        MatConf = np.zeros([nbClass + 1, nbClass + 1])  # +1: non-action,
        TP = np.zeros([nbClass + 1])
        FP = np.zeros([nbClass + 1])
        nbPerclasses = np.zeros([nbClass + 1])
        # knowing the nb of examples per class allows to compute the recall
        for label in sequence.labels:
            classIDgt = label.classID
            nbPerclasses[classIDgt] += 1

        for labelPred in prediction:
            classId = labelPred.classID
            if classId == 0:
                print("Warning: classID = 0 found in the prediction, skipping")
                continue

            # get the GT which match the best the given prediction
            idLabel, labelGT = self.findCorrepondingGT(labelPred, sequence.labels, self.deltaFrames,
                                                       self.useStartFrameInsteadOfNegativeDelta)
            if labelGT == -1:
                FP[classId] += 1
                MatConf[0, classId] += 1
                continue

            if classId == labelGT:
                # accordingly to the original paper [Fothergill2012], no FP is counted if already flagged, just ignored
                if flags[idLabel] == 0:
                    TP[classId] += 1
                    MatConf[classId, classId] += 1
                    flags[idLabel] = 1
            else:
                FP[classId] += 1
                MatConf[labelGT][classId] += 1

        return TP, FP, nbPerclasses, MatConf

    def AggregateMultiFold(self, resultsFolds: List, pathResult: str, filesInFold:List[str]):
        """
        Aggregate the results of the multi-fold cross-validation using macro-averaging
        :param filesInFold:
        :param resultsFolds: the TP,FP and count for each fold
        :return:
        """
        Precision_c_mf = []
        Recall_c_mf = []
        F1_c_mf = []
        Precision_mf = []
        Recall_mf = []
        F1_mf = []
        for TP_c, FP_c, Count_c in resultsFolds:
            Precision_c, Recall_c, F1_c, Precision, Recall, F1, _, _, _ = self.GetPrecisionRecallF1(TP_c, FP_c,
                                                                                                    Count_c)
            Precision_c_mf.append(Precision_c)
            Recall_c_mf.append(Recall_c)
            F1_c_mf.append(F1_c)
            Precision_mf.append(Precision)
            Recall_mf.append(Recall)
            F1_mf.append(F1)

        stdF1_mf = np.std(F1_mf)
        stdPrecision_mf = np.std(Precision_mf)
        stdRecall_mf = np.std(Recall_mf)

        Precision_c_mf = np.average(Precision_c_mf, axis=0)
        Recall_c_mf = np.average(Recall_c_mf, axis=0)
        F1_c_mf = np.average(F1_c_mf, axis=0)
        Precision_mf = np.average(Precision_mf, axis=0)
        Recall_mf = np.average(Recall_mf, axis=0)
        F1_per_fold = F1_mf
        F1_mf = np.average(F1_mf, axis=0)


        resultSTR = self.GetDescriptiveString(self.nbClass, None, None, None, Precision_c_mf, Recall_c_mf, F1_c_mf,
                                              Precision_mf, Recall_mf, F1_mf, None, None, None, None, None, stdPrecision_mf,
                                              stdRecall_mf, stdF1_mf, None)
        resultSTR += "F1 per fold: \n"
        for i in range(len(F1_per_fold)):
            resultSTR += str(i)
            try:
                resultSTR += ". " + str(filesInFold[i])+" "
            except  Exception as e:
                raise e
            resultSTR+= "\t :  " + ("%.5f" % F1_per_fold[i]) + "\n"

        strUseStartFrameToEvaluate = "useStartFrame" if self.useStartFrameInsteadOfNegativeDelta else "useActionPointLessDelta"
        f = open(
            pathResult + self.name + "_" + strUseStartFrameToEvaluate + "_" + str(self.deltaFrames) + "_MultiFold.txt",
            "w+")
        f.write(resultSTR)
        f.close()

    def ExportPerformance(self, TP_per_class, FP_per_class, Count_per_class, total_MatConf,
                          path):
        """
        Same use for individual sequences and for the whole test set
        :param TP_per_class:
        :param FP_per_class:
        :param Count_per_class:
        :param path: path to export the results
        :return:
        """

        Precision_c, Recall_c, F1_c, Precision, Recall, F1, TPAll, FPAll, CountAll = self.GetPrecisionRecallF1(
            TP_per_class, FP_per_class,
            Count_per_class)
        # self.resultsToSummarize["IoU_F1"] = (self.alpha, F1)

        # print(sum([total_MatConf[i][i] for i in range(self.nbClass+1)]),TPAll)
        # print(total_MatConf)
        assert sum([total_MatConf[i][i] for i in range(self.nbClass + 1)]) == TPAll
        try:
            # fig, ax = plt.subplot(figsize=(15, 15))
            disp = ConfusionMatrixDisplay(confusion_matrix=total_MatConf,
                                          display_labels=[i for i in
                                                          range(len(TP_per_class))])
            disp.plot(include_values=True, cmap=plt.cm.Blues, xticks_rotation="vertical", values_format=".1f")
            plt.savefig(path[:-4] + "_ConfMatrix.png")
            plt.close()
        except Exception as e:
            print(e)
            print("Confusion matrix not exported")

        resultSTR = self.GetDescriptiveString(self.nbClass, TP_per_class, FP_per_class, Count_per_class,
                                              Precision_c,
                                              Recall_c, F1_c,
                                              Precision, Recall, F1, TPAll, FPAll, CountAll)
        resultSTR = "Multi-fold results:\n" + resultSTR

        with open(path, "w+") as f:
            f.write(resultSTR)
            f.close()

    def findCorrepondingGT(self, labelPred: Label, labels: List[Label], deltaFrames: int,
                           useStartFrameInsteadOfNegativeDelta: bool):
        """
        Find the GT which match the start frame of prediction
        ie [startFrame,ActionPoint+deltaFrames] if useStartFrameInsteadOfNegativeDelta is True
        else [ActionPoint-deltaFrames,ActionPoint+deltaFrames]
        :param labelPred:
        :param labels:
        :param deltaFrames:
        :param useStartFrameInsteadOfNegativeDelta:
        :return:
        """
        for index, labGT in enumerate(labels):
            useAsActionPoint = labGT.actionPoint if labGT.actionPoint is not None else labGT.endFrame
            comparativeStartFrame = labGT.startFrame - deltaFrames if useStartFrameInsteadOfNegativeDelta \
                else max(labGT.startFrame, useAsActionPoint - deltaFrames)
            if comparativeStartFrame <= labelPred.startFrame <= useAsActionPoint + deltaFrames:
                return index, labGT.classID
        return -1, -1
