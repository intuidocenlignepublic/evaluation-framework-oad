import os
from typing import List, Tuple, Any

import numpy as np
from matplotlib import pyplot as plt

from Classes.Label import Label
from Classes.Sequence import Sequence
from Metrics.BoundsBasedMetrics.BoundsBasedMetric import BoundsBasedMetric
from Metrics.ExistingResult import ExistingResult, CURRENT_APPROACH_NAME


class DetectionToActionPoint(BoundsBasedMetric):
    """
    This is the metric used in the paper of Boulahia, RFIAP 2018. (hardest protocol)

    action C1 action is considered, start from AP-26 to AP+15
    Example :
        À AP-15, detect C2.
        À AP-7, detect C1.
        À AP-2, detect C1.
        À AP+6, detect C1.
        À AP+10 and before the end, detect C2
    Then :
        - for time AP-26: we have : TP = 0 , FP=5 et FN=1
        - for time AP-20: we have : TP = 0 , FP=5 et FN=1
        - for time AP-15: we have : TP = 0 , FP=5 et FN=1
        - for time AP-10: we have : TP = 0 , FP=5 et FN=1
        - for time AP-7 (and after): we have : TP = 1 , FP=4 FN=0

    :param nbClass: the number of classes
    :return:Tp,FP,FN
    """

    def __init__(self, nbClass, exportPerSequenceDetailResult):
        super().__init__("DetectionToActionPoint")
        self.nbClass = nbClass
        self.exportPerSequenceDetailResult = exportPerSequenceDetailResult

        self.nbFrameBefore = 40
        self.nbFrameAfter = 0

    def Evaluate(self, sequence: List[Sequence], prediction: List[List[Label]], pathExport: str):
        """

        :param sequence:
        :param prediction:
        :param pathExport:
        :return:
        """
        nbAction = np.zeros([self.nbFrameBefore + 1 + self.nbFrameAfter, self.nbClass + 1])
        FalseNegative = np.zeros([self.nbFrameBefore + 1 + self.nbFrameAfter, self.nbClass + 1])
        FalsePositive = np.zeros([self.nbFrameBefore + 1 + self.nbFrameAfter, self.nbClass + 1])
        TruePositive = np.zeros([self.nbFrameBefore + 1 + self.nbFrameAfter, self.nbClass + 1])

        for i, seq in enumerate(sequence):
            TruePositive_seq, FalsePositive_seq, FalseNegative_seq, nbAction_seq = self.EvaluateSequence(seq,
                                                                                                         prediction[i])
            TruePositive += TruePositive_seq
            FalsePositive += FalsePositive_seq
            FalseNegative += FalseNegative_seq
            nbAction += nbAction_seq
            if self.exportPerSequenceDetailResult:
                pathSeq = pathExport + "/Sequence/" + sequence[i].name + "/"
                if not os.path.exists(pathSeq):
                    os.makedirs(pathSeq)
                self.ExportPerformance(TruePositive_seq, FalsePositive_seq, FalseNegative_seq, nbAction_seq,
                                       pathSeq + "/" + self.name + ".txt", exportGraphic=False)

        self.ExportPerformance(TruePositive, FalsePositive, FalseNegative, nbAction,
                               pathExport + "/" + self.name + ".txt")
        return nbAction, FalseNegative, FalsePositive, TruePositive

    def findClassFiredDuringBound(self, prediction: List[Label], actionStartFrame, actionEndFrame) -> List[
        Tuple[int, int]]:
        """
        Very important, find all the classes that have been emitted since the begin of the action (actionStartCuDiFrame)
        until the CuDi frame index (current observed frame)
        :param actionStartCuDiFrame:
        :param CuDiFrameIndex:
        :return: all the classes found (id), and the correpoding time
        """
        classesFound: List[Tuple[int, int]] = []
        for classID, start, end in list(map(lambda p: (p.classID, p.startFrame, p.endFrame), prediction)):
            # if the action bound has started after the action start,
            # NO need to check if the  current observed frame is still
            # concerned by this bound (before the end) because we want to get all the classes
            # and the 0 actions bounds are emitted by the Strategy
            if actionStartFrame <= start <= actionEndFrame:
                classesFound.append((classID, start))
        return classesFound  # in the order, the last one is the class of the current CuDiFrameIndex

    def EvaluateSequence(self, sequence: Sequence, prediction: List[Label]):
        nbAction = np.zeros([self.nbFrameBefore + 1 + self.nbFrameAfter, self.nbClass + 1])
        FalseNegative = np.zeros([self.nbFrameBefore + 1 + self.nbFrameAfter, self.nbClass + 1])
        FalsePositive = np.zeros([self.nbFrameBefore + 1 + self.nbFrameAfter, self.nbClass + 1])
        TruePositive = np.zeros([self.nbFrameBefore + 1 + self.nbFrameAfter, self.nbClass + 1])
        for idActPoint, (actId, actStart, actPoint, actEnd) in enumerate(map(
                lambda l: (l.classID, l.startFrame, l.actionPoint, l.endFrame), sequence.labels)):
            # print("\n---------\nACTION n°",idActPoint)
            if actPoint is None:
                actPoint = actEnd
            if not (actStart <= actPoint <= actEnd):
                # print(" actStart,actPoint,actEnd", actStart, actPoint, actEnd, ':act point reduced to actEnd')
                actPoint = actEnd
            assert (actStart <= actPoint <= actEnd)
            temporalframesConcerned = [i for i in range(max(0, actPoint - self.nbFrameBefore),
                                                        actPoint + self.nbFrameAfter + 1)]  # temporal domain
            for idFrameObsRatio, endFractionObsFrameTemporal__T in enumerate(temporalframesConcerned):
                # if it's actPoint-20, then put it in index 0
                # 20 - (1500 - (1500 - 20)) = 0
                # 20-(1500-(1500-19))=1
                indext = (self.nbFrameBefore) - (actPoint - endFractionObsFrameTemporal__T)

                if actId == 0:  # if classs 0, depends of the annotation
                    raise Exception("should not be the case, review. "
                                    "The classes ids are not 0 based, 0 is kept for the background activity")

                classesFound: List[Tuple[int, int]] = self.findClassFiredDuringBound(prediction, actStart,
                                                                                     actEnd)  # return (classe,time)
                nbAction[indext][actId] += 1
                if len(classesFound) == 0:
                    FalseNegative[indext][actId] += 1
                else:
                    detectionsDoneBeforeAPLessT = [[classe, time] for id, (classe, time) in enumerate(classesFound) if
                                                   time <= endFractionObsFrameTemporal__T]
                    if len(detectionsDoneBeforeAPLessT) == 0 or \
                            actId not in np.array(detectionsDoneBeforeAPLessT)[:, 0]:
                        FalseNegative[indext][actId] += 1

                    for i in range(len(classesFound)):
                        if classesFound[i][0] == actId and \
                                (actId not in np.array(classesFound)[:i, 0]) and \
                                classesFound[i][1] <= endFractionObsFrameTemporal__T:
                            # if the predicted class is the correct one
                            # and if the correct class has not been predicted before
                            # and if the prediction is before the current moment (endFractionObsFrameTemporal__T)
                            TruePositive[indext][actId] += 1
                        else:
                            # quite hard penalty for the last condition (class fired after the evaluation moment)
                            # but this is how it was designed by Boulahia (see mail exchanges)
                            # and it guarantes that the curve is always increasing
                            FalsePositive[indext][actId] += 1

        return TruePositive, FalsePositive, FalseNegative, nbAction

    def getPrintableResultERBoulahia(self, TruePositive: np.ndarray, FalsePositive: np.ndarray,
                                     FalseNegative: np.ndarray,
                                     nbAction: np.ndarray) -> Tuple[str, Any, Any, Any, Any]:
        precisionsAveragePerFrame = []
        recallsAveragePerFrame = []
        fmeasureAveragePerFrame = []
        accuracyAveragePerFrame = []
        res = "----Results----"
        for frameCompletion in range(self.nbFrameBefore + 1 + self.nbFrameAfter):
            # print("frameCompletion",frameCompletion)
            res += "\nT-" + str(self.nbFrameBefore - frameCompletion) + "\n"
            TP = TruePositive[frameCompletion]  # vector of size nbClass
            FP = FalsePositive[frameCompletion]
            FN = FalseNegative[frameCompletion]
            nbAct = nbAction[frameCompletion]

            assert nbAct[0] == 0
            # assert np.array_equal(TP+FP+FN,nbAct)
            # TP = TP[1:]
            # FP = FP[1:]
            # FN = FN[1:]
            # nbAct = nbAct[1:]
            # theses are vector of size [nbClass]
            precisionForFramePerClass = np.divide(TP, TP + FP, out=np.zeros_like(TP), where=TP + FP != 0)
            # precisionForFramePerClass = TP/(TP+FP) # TP/PredictedPositiv
            recallForFramePerClass = np.divide(TP, TP + FN, out=np.zeros_like(TP), where=TP + FN != 0)
            # recallForFramePerClass = TP/(TP+FN) # TP/RealPositiv
            numer = 2 * precisionForFramePerClass * recallForFramePerClass
            denome = (precisionForFramePerClass + recallForFramePerClass)
            fmeasureForFramePerClass = np.divide(numer, denome, out=np.zeros_like(numer), where=denome != 0)
            # fmeasureForFramePerClass = 2*precisionForFramePerClass*recallForFramePerClass/(precisionForFramePerClass+recallForFramePerClass)
            accuracyForFramePerClass = np.divide(TP, nbAct, out=np.zeros_like(TP), where=nbAct != 0)
            # accuracyForFramePerClass = TP/nbAct # is equivalent to TAR, TP/All
            # print("nbAct",nbAct)
            # print("accuracyForFramePerClass",accuracyForFramePerClass)
            # print("TP",TP)
            # print("FP",FP)
            # print("FN",FN)

            res += "TP,\n" + str(" ; ".join(list(["{:.1f}".format(v) for v in TP]))) + "\n"
            res += "FP,\n" + str(" ; ".join(list(["{:.1f}".format(v) for v in FP]))) + "\n"
            res += "FN,\n" + str(" ; ".join(list(["{:.1f}".format(v) for v in FN]))) + "\n"
            res += "nbAct,\n" + str(" ; ".join(list(["{:.1f}".format(v) for v in nbAct]))) + "\n"
            res += "precisionPerClass,\n" + str(
                " ; ".join(list(["{:.2f}".format(v) for v in precisionForFramePerClass]))) + "\n"
            res += "recallPerClass,\n" + str(
                " ; ".join(list(["{:.2f}".format(v) for v in recallForFramePerClass]))) + "\n"
            res += "fmeasurePerClass,\n" + str(
                " ; ".join(list(["{:.2f}".format(v) for v in fmeasureForFramePerClass]))) + "\n"
            res += "accuracyPerClass,\n" + str(
                " ; ".join(list(["{:.2f}".format(v) for v in accuracyForFramePerClass]))) + "\n"

            # micro average
            TPsum = sum(TP)
            FPsum = sum(FP)
            FNsum = sum(FN)
            actionsSum = sum(nbAct)
            precisionForFrame = TPsum / (TPsum + FPsum) if TPsum + FPsum != 0 else 0
            recallForFrame = TPsum / (TPsum + FNsum) if TPsum + FNsum != 0 else 0
            fmeasureForFrame = 2 * precisionForFrame * recallForFrame / \
                               (precisionForFrame + recallForFrame) if precisionForFrame + recallForFrame != 0 else 0
            # print("actionsSum",actionsSum)
            # print("TPsum",TPsum)
            # print("FPsum",FPsum)
            # print("FNsum",FNsum)
            # is equivalent to TAR, TP/All
            accuracyForFrame = TPsum / (TPsum + FNsum + FPsum) if TPsum + FNsum + FPsum != 0 else 0
            precisionsAveragePerFrame.append(precisionForFrame)
            recallsAveragePerFrame.append(recallForFrame)
            fmeasureAveragePerFrame.append(fmeasureForFrame)
            accuracyAveragePerFrame.append(accuracyForFrame)

        # print("accuracyAveragePerFrame",accuracyAveragePerFrame)
        # print("fmeasureAveragePerFrame",fmeasureAveragePerFrame)
        res += "\n"
        res += "PerFrame micro-average\n"
        res += "precisionPerFrame,\n" + str(
            " ; ".join(list(["{:.2f}".format(v) for v in precisionsAveragePerFrame]))) + "\n"
        res += "recallPerFrame,\n" + str(" ; ".join(list(["{:.2f}".format(v) for v in recallsAveragePerFrame]))) + "\n"
        res += "fmeasurePerFrame,\n" + str(
            " ; ".join(list(["{:.2f}".format(v) for v in fmeasureAveragePerFrame]))) + "\n"
        res += "accuracyPerFrame,\n" + str(
            " ; ".join(list(["{:.2f}".format(v) for v in accuracyAveragePerFrame]))) + "\n"
        return res, precisionsAveragePerFrame, recallsAveragePerFrame, fmeasureAveragePerFrame, accuracyAveragePerFrame

    def ExportPerformance(self, TruePositive, FalsePositive, FalseNegative, nbAction, pathOutput, exportGraphic=True):
        res, precisionsAveragePerFrame, recallsAveragePerFrame, \
        fmeasureAveragePerFrame, accuracyAveragePerFrame = self.getPrintableResultERBoulahia(TruePositive,
                                                                                             FalsePositive,
                                                                                             FalseNegative,
                                                                                             nbAction)
        precisionForFramesPerClass = np.divide(TruePositive, TruePositive + FalsePositive,
                                               out=np.zeros_like(TruePositive),
                                               where=TruePositive + FalsePositive != 0)
        recallForFramesPerClass = np.divide(TruePositive, TruePositive + FalseNegative, out=np.zeros_like(TruePositive),
                                            where=TruePositive + FalseNegative != 0)
        # precisionForFramesPerClass = TruePositive / (TruePositive + FalsePositive)  # TP/PredictedPositiv
        # recallForFramesPerClass = TruePositive / (TruePositive + FalseNegative)  # TP/RealPositiv
        fmeasureForFramesPerClass = np.divide(2 * precisionForFramesPerClass * recallForFramesPerClass,
                                              precisionForFramesPerClass + recallForFramesPerClass,
                                              out=np.zeros_like(precisionForFramesPerClass),
                                              where=precisionForFramesPerClass + recallForFramesPerClass != 0)
        # fmeasureForFramesPerClass = 2 * precisionForFramesPerClass * recallForFramesPerClass / (
        #         precisionForFramesPerClass + recallForFramesPerClass)
        accuracyAveragePerFrame = np.divide(TruePositive, TruePositive + FalseNegative + FalsePositive,
                                            out=np.zeros_like(TruePositive),
                                            where=TruePositive + FalseNegative + FalsePositive != 0)
        # accuracyForFramesPerClass = TruePositive / (
        #         TruePositive + FalseNegative + FalsePositive)  # Accuracy using /nbAction is not very relevent since we can have more than 1 FP per sample

        with open(pathOutput, "w+") as f:
            f.write(res)
        if exportGraphic:

            absci = range(-self.nbFrameBefore, self.nbFrameAfter + 1)
            plt.plot(absci, fmeasureAveragePerFrame, color="red")
            # plt.plot(absci, boulahiaResult, color="blue")
            plt.xlabel('Distance to action point')
            plt.ylabel('F-Measure')
            plt.legend([CURRENT_APPROACH_NAME.NAME])
            plt.grid(linewidth=0.25)
            plt.savefig(pathOutput[:-4], bbox_inches='tight')
            # plt.savefig(pathOutput[:-4], bbox_inches='tight', format='eps')
            legend = [CURRENT_APPROACH_NAME.NAME]
            existingResults = ExistingResult.getExistingResultsForMetric(self.name)
            if existingResults is not None:
                try:
                    for existingResult in existingResults:
                        legend.append(existingResult["name"])
                        if "color" in existingResult:
                            plt.plot(existingResult["abciss"], existingResult["results"], color=existingResult["color"])
                        else:
                            plt.plot(existingResult["abciss"], existingResult["results"])
                except TypeError:
                    print(
                        "The furnished existing results are not iterable, give a list of existing results for metric ",
                        self.name)
                plt.legend(legend)
                plt.savefig(pathOutput[:-4] + "Comparison", bbox_inches='tight')

            plt.close()

            plt.figure(figsize=(10, 5.5), dpi=180)
            i = 0
            legende: List[str] = []
            for tableFrame in np.transpose(fmeasureForFramesPerClass):
                plt.plot([u for u in absci], tableFrame)
                legende.append(str(i))
                i += 1
            plt.legend(legende)
            plt.grid(linewidth=0.25)
            plt.savefig(pathOutput[:-4] + "fmeasureForFramesPerClass", bbox_inches='tight')
            # plt.savefig(pathOutput[:-4] + "fmeasureForFramesPerClass", bbox_inches='tight', format='eps')
            plt.close()

    def AggregateMultiFold(self, resultsFolds: List, pathResult: str, filesInFold):
        TruePositive = np.zeros((self.nbFrameBefore + self.nbFrameAfter + 1,self.nbClass+1))
        FalsePositive = np.zeros((self.nbFrameBefore + self.nbFrameAfter + 1,self.nbClass+1))
        FalseNegative = np.zeros((self.nbFrameBefore + self.nbFrameAfter + 1,self.nbClass+1))
        nbAction = np.zeros((self.nbFrameBefore + self.nbFrameAfter + 1,self.nbClass+1))
        for i in range(len(resultsFolds)): # order is nbAction, FalseNegative, FalsePositive, TruePositive
            nbAction += resultsFolds[i][0]
            FalseNegative += resultsFolds[i][1]
            FalsePositive += resultsFolds[i][2]
            TruePositive += resultsFolds[i][3]
        self.ExportPerformance(TruePositive, FalsePositive, FalseNegative, nbAction, pathResult + self.name +"MF.txt")

