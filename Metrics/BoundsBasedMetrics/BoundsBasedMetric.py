from abc import abstractmethod
from typing import List

from Classes.Label import Label
from Classes.Sequence import Sequence
import numpy as np

from Metrics.Metric import Metric
from sklearn.metrics import precision_score, recall_score


class BoundsBasedMetric(Metric):
    def __init__(self, name):
        super().__init__(name)
        self.name = name

    @abstractmethod
    def Evaluate(self, sequence: List[Sequence], prediction: List[List[Label]], pathExport: str):
        pass

    @abstractmethod
    def EvaluateSequence(self, sequence: Sequence, prediction: List[Label]):
        pass

    def GetPrecisionRecallF1(self, TP_per_class, FP_per_class, Count_per_class):
        """
        Compute the precision, recall and F1 score for each class
        Precision = TP / (TP + FP), Recall = TP / (COUNT), F1 = 2 * (Precision * Recall) / (Precision + Recall)
        if TP+FP = 0 or T, or COUNT=0, then Fscore is set to 0
        use Micro-average to compute the overall score
        :param TP_per_class:
        :param FP_per_class:
        :param Count_per_class:
        :return:
        """
        Precision_c = np.divide(TP_per_class, TP_per_class + FP_per_class, out=np.array([0.] * len(TP_per_class)),
                                where=TP_per_class + FP_per_class != 0)
        Recall_c = np.divide(TP_per_class, Count_per_class, out=np.array([0.] * len(TP_per_class)),
                             where=Count_per_class != 0)
        F1_c = np.array([0.] * len(TP_per_class))
        for i in range(len(Precision_c)):
            if Precision_c[i] != 0 and Recall_c[i] != 0:
                F1_c[i] = 2.0 * Precision_c[i] * Recall_c[i] / (Precision_c[i] + Recall_c[i])
            else:
                F1_c[i] = 0.0

        TPAll = np.sum(TP_per_class)
        FPAll = np.sum(FP_per_class)
        CountAll = np.sum(Count_per_class)
        Precision = TPAll / (TPAll + FPAll) if TPAll + FPAll != 0 else 0.0
        Recall = TPAll / CountAll if CountAll != 0 else 0.0
        F1 = 2 * Precision * Recall / (Precision + Recall) if Precision != 0 and Recall != 0 else 0.0
        return Precision_c, Recall_c, F1_c, Precision, Recall, F1, TPAll, FPAll, CountAll

    def GetDescriptiveString(self, nbClass, TP_per_class, FP_per_class, Count_per_class, Precision_c, Recall_c, F1_c,
                             Precision, Recall, F1, TPAll, FPAll, CountAll, NTtoD_per_class=None, NTtoD_avg=None,
                             stdPrecision_mf=None, stdRecall_mf=None, stdF1_mf=None, stdNTtoD_mf=None):
        space = 6
        resultSTR = "Per class score : \n"
        resultSTR += "class              : " + " ; ".join(
            [str(i) + " " * (space - len(str(i))) for i in range(nbClass + 1)]) + "\n"
        if TP_per_class is not None:
            resultSTR += "TP per class       : " + " ; ".join(
                [str(v) + " " * (space - len(str(v))) for v in TP_per_class]) + "\n"
            resultSTR += "FP per class       : " + " ; ".join(
                [str(v) + " " * (space - len(str(v))) for v in FP_per_class]) + "\n"
            resultSTR += "Count per class    : " + " ; ".join(
                [str(c) + " " * (space - len(str(c))) for c in Count_per_class]) + "\n"
        resultSTR += "Precision per class: " + " ; ".join(
            [("%.3f" % v) + " " * (space - len("%.3f" % v)) if v is not None else "None" + " " * (space - 4) for v in
             Precision_c]) + "\n"
        resultSTR += "Recall    per class: " + " ; ".join(
            [("%.3f" % v) + " " * (space - len("%.3f" % v)) if v is not None else "None" + " " * (space - 4) for v in
             Recall_c]) + "\n"
        resultSTR += "FScore    per class: " + " ; ".join(
            [("%.3f" % v) + " " * (space - len("%.3f" % v)) if v is not None else "None" + " " * (space - 4) for v in
             F1_c]) + "\n"
        if NTtoD_per_class is not None:
            # print("total_NTtoD_per_class", total_NTtoD_per_class)
            resultSTR += "NTtoD     per class: " + " ; ".join(
                [("%.3f" % v) + " " * (space - len("%.3f" % v)) if v is not None else "None" + " " * (space - 4) for v
                 in NTtoD_per_class]) + "\n"
        resultSTR += "\n"
        resultSTR += "Overall score: \n"
        if TPAll is not None:
            resultSTR += "TP:        " + str(TPAll) + "\n"
            resultSTR += "FP:        " + str(FPAll) + "\n"
            resultSTR += "Count:     " + str(CountAll) + "\n"
        stringSTD = " +- %.3f" % stdPrecision_mf if stdPrecision_mf is not None else ""
        resultSTR += "Precision: " + ("%.3f" % Precision if Precision is not None else "None") + stringSTD + "\n"
        stringSTD = " +- %.3f" % stdRecall_mf if stdRecall_mf is not None else ""
        resultSTR += "Recall:    " + ("%.3f" % Recall if Recall is not None else "None") + stringSTD + "\n"
        stringSTD = " +- %.3f" % stdF1_mf if stdF1_mf is not None else ""
        resultSTR += "FScore:    " + ("%.3f" % F1 if F1 is not None else "None") + stringSTD + "\n"
        if NTtoD_avg is not None:
            stringSTD = " +- %.3f" % stdNTtoD_mf if stdNTtoD_mf is not None else ""
            resultSTR += "NTtoD:     " + ("%.3f" % NTtoD_avg if NTtoD_avg is not None else "None") + stringSTD + "\n"
        return resultSTR
