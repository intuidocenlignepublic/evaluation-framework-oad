import math
import os
from typing import List

from Classes.Label import Label
from Classes.Sequence import Sequence
from Metrics.BoundsBasedMetrics.BoundsBasedMetric import BoundsBasedMetric
import numpy as np
from matplotlib import pyplot as plt
from sklearn.metrics import ConfusionMatrixDisplay


class BoundedOfflineOverlap(BoundsBasedMetric):
    """
    Metric used in the paper
    Li, Y., Lan, C., Xing, J., Zeng, W., Yuan, C., Liu, J. (2016).
    Online Human Action Detection Using Joint Classification-Regression Recurrent Neural Networks.
    In: ECCV 2016.
    https://doi.org/10.1007/978-3-319-46478-7_13
    """

    def __init__(self, alpha, nbClass, exportPerSequenceDetailResult):
        """

        :param alpha: minimun overlap between the two bounds to be considered as a match (TP)
        """
        super().__init__("BoundedOfflineOverlap")
        self.exportPerSequenceDetailResult = exportPerSequenceDetailResult
        self.nbClass = nbClass
        self.alpha = alpha

    def Evaluate(self, sequence: List[Sequence], prediction: List[List[Label]], pathExport):
        """
        :param sequence: list of sequences
        :param prediction: list of list of labels
        :return:
        """
        assert len(sequence) == len(prediction)
        total_TP_per_class = np.zeros([self.nbClass + 1])
        total_FP_per_class = np.zeros([self.nbClass + 1])
        total_Count_per_class = np.zeros([self.nbClass + 1])
        total_matconf = np.zeros([self.nbClass + 1, self.nbClass + 1])
        SL_Score = 0
        EL_Score = 0
        if not os.path.exists(pathExport):
            os.makedirs(pathExport)
        for i in range(len(sequence)):
            TP_per_class, FP_per_class, Count_per_class, Mat_conf, SL, EL = self.EvaluateSequence(sequence[i],
                                                                                                  prediction[i])

            if self.exportPerSequenceDetailResult:
                pathSeq = pathExport + "/Sequence/" + sequence[i].name + "/"
                if not os.path.exists(pathSeq):
                    os.makedirs(pathSeq)

                self.ExportPerformance(TP_per_class, FP_per_class, Count_per_class, Mat_conf,
                                       SL / sum(Count_per_class), EL / sum(Count_per_class),
                                       pathSeq + self.name + str(self.alpha) + ".txt")

            total_TP_per_class += TP_per_class
            total_FP_per_class += FP_per_class
            total_Count_per_class += Count_per_class
            total_matconf += Mat_conf
            SL_Score += SL
            EL_Score += EL
        SL_Score /= sum(total_Count_per_class)  # divide by the total number of labels, since its 0 for FN
        EL_Score /= sum(total_Count_per_class)
        assert SL_Score <= 1

        self.ExportPerformance(total_TP_per_class, total_FP_per_class, total_Count_per_class, total_matconf,
                               SL_Score, EL_Score,
                               pathExport + "/" + self.name + str(self.alpha) + ".txt")
        # precision_per_class = total_TP_per_class / (total_TP_per_class + total_FP_per_class)
        # recall_per_class = total_TP_per_class / (total_TP_per_class + total_Count_per_class)
        # F1_per_class = 2 * precision_per_class * recall_per_class / (precision_per_class + recall_per_class)
        return total_TP_per_class, total_FP_per_class, total_Count_per_class, SL_Score, EL_Score

    def IntersectionOverUnion(self, label1: Label, label2: Label):
        """
        the order doesn't matter
        :param label1: first label
        :param label2: second label
        :return:
        """
        maxiStart = max(label1.startFrame, label2.startFrame)
        miniEnd = min(label1.endFrame, label2.endFrame)

        if miniEnd < maxiStart:  # no overlap
            return 0
        intersection = miniEnd - maxiStart + 1

        areaPred = (label1.endFrame - label1.startFrame) + 1
        areaGT = (label2.endFrame - label2.startFrame) + 1
        union = areaPred + areaGT - intersection
        # print(intersection, union,intersection/ union)
        return intersection / union

    def EvaluateSequence(self, sequence: Sequence, prediction: List[Label]):
        """
        :param sequence: sequence, contain Class and Bounds
        :param prediction: list of labels [ClassID, startFrame, endFrame]
        :return:
        """
        nbClass = self.nbClass
        flags = np.zeros([len(sequence.labels)])
        sequence.labels = sorted(sequence.labels, key=lambda x: x.startFrame)
        MatConf = np.zeros([nbClass + 1, nbClass + 1])  # +1: non-action,
        TP = np.zeros([nbClass + 1])
        FP = np.zeros([nbClass + 1])
        nbPerclasses = np.zeros([nbClass + 1])

        SL = 0
        EL = 0

        # knowing the nb of examples per class allows to compute the recall
        for label in sequence.labels:
            classIDgt = label.classID
            nbPerclasses[classIDgt] += 1
        for labelPred in prediction:
            classId = labelPred.classID
            if classId == 0:
                print("Warning: classID = 0 found in the prediction, skipping")
                continue

            # get the GT which match the best the given prediction
            gtIndex = int(np.argmax(list(
                map(lambda lab: self.IntersectionOverUnion(labelPred, lab),
                    sequence.labels))))
            gtMax = np.max(list(
                map(lambda lab: self.IntersectionOverUnion(labelPred, lab),
                    sequence.labels)))

            # print(gtIndex, gtMax)
            classIdGT = sequence.labels[gtIndex].classID
            if flags[gtIndex] == 0 and classId == classIdGT and gtMax > self.alpha:
                flags[gtIndex] = 1
                TP[classIdGT] += 1
                MatConf[classIdGT][classIdGT] += 1
                stGT = sequence.labels[gtIndex].startFrame
                endGT = sequence.labels[gtIndex].endFrame
                SL_loc = math.exp(-math.fabs(labelPred.startFrame - stGT) / (endGT - stGT))
                SL += SL_loc
                EL_loc = math.exp(-math.fabs(labelPred.endFrame - endGT) / (endGT - stGT))
                EL += EL_loc
            else:  # if gtMax>self.alpha:
                FP[classId] += 1
                # if gtMax > 0:  # if there is no overlap with any GT bounds (took the first GT in the argmax)
                #     flags[gtIndex] = 1
                if classId != classIdGT and gtMax > 0:
                    MatConf[classIdGT][classId] += 1
                else:
                    MatConf[0][classId] += 1
        # assert (countSL<=sum(nbPerclasses))
        return TP, FP, nbPerclasses, MatConf, SL, EL

    def ExportPerformance(self, TP_per_class, FP_per_class, Count_per_class, total_MatConf, SL_Score, EL_Score, path):
        """
        Same use for individual sequences and for the whole test set
        :param TP_per_class:
        :param FP_per_class:
        :param Count_per_class:
        :param path: path to export the results
        :return:
        """

        Precision_c, Recall_c, F1_c, Precision, Recall, F1, TPAll, FPAll, CountAll = self.GetPrecisionRecallF1(
            TP_per_class, FP_per_class,
            Count_per_class)
        self.resultsToSummarize["IoU_F1"] = (self.alpha, F1)
        # print(sum([total_MatConf[i][i] for i in range(self.nbClass+1)]),TPAll)
        # print(total_MatConf)
        assert sum([total_MatConf[i][i] for i in range(self.nbClass + 1)]) == TPAll
        try:
            disp = ConfusionMatrixDisplay(confusion_matrix=total_MatConf,
                                          display_labels=[i for i in
                                                          range(len(TP_per_class))])
            fig, ax = plt.subplots(figsize=(15, 15))
            disp.plot(include_values=True, cmap=plt.cm.Blues, xticks_rotation="vertical", values_format=".0f", ax=ax)
            plt.savefig(path[:-4] + "_ConfMatrix.png")
            plt.close()
            plt.close(fig)
        except Exception as e:
            print(e)
            print("Confusion matrix not exported")

        resultSTR = self.GetDescriptiveString(self.nbClass, TP_per_class, FP_per_class, Count_per_class, Precision_c,
                                              Recall_c, F1_c,
                                              Precision, Recall, F1, TPAll, FPAll, CountAll)
        resultSTR += "SL Score:  " + ("%.3f" % SL_Score) + "\n"
        resultSTR += "EL Score:  " + ("%.3f" % EL_Score) + "\n"

        with open(path, "w+") as f:
            f.write(resultSTR)
            f.close()

    def AggregateMultiFold(self, resultsFolds: List, pathResult: str, filesInFold):
        """
        Aggregate the results of the multi-fold cross-validation using macro-averaging
        :param filesInFold:
        :param resultsFolds: the TP,FP and count for each fold
        :return:
        """
        Precision_c_mf = []
        Recall_c_mf = []
        F1_c_mf = []
        Precision_mf = []
        Recall_mf = []
        F1_mf = []
        SL = []
        EL = []
        for TP_c, FP_c, Count_c, SL_Score, EL_Score in resultsFolds:
            Precision_c, Recall_c, F1_c, Precision, Recall, F1, _, _, _ = self.GetPrecisionRecallF1(TP_c, FP_c, Count_c)
            Precision_c_mf.append(Precision_c)
            Recall_c_mf.append(Recall_c)
            F1_c_mf.append(F1_c)
            Precision_mf.append(Precision)
            Recall_mf.append(Recall)
            F1_mf.append(F1)
            SL.append(SL_Score)
            EL.append(EL_Score)

        stdDevSL = np.std(SL)
        stdDevEL = np.std(EL)
        SL = np.average(SL)
        EL = np.average(EL)

        stdPrecision_mf = np.std(Precision_mf)
        stdRecall_mf = np.std(Recall_mf)
        stdF1_mf = np.std(F1_mf)

        Precision_c_mf = np.average(Precision_c_mf, axis=0)
        Recall_c_mf = np.average(Recall_c_mf, axis=0)
        F1_c_mf = np.average(F1_c_mf, axis=0)
        Precision_mf = np.average(Precision_mf, axis=0)
        Recall_mf = np.average(Recall_mf, axis=0)
        F1_perfold = F1_mf
        F1_mf = np.average(F1_mf, axis=0)

        resultSTR = self.GetDescriptiveString(self.nbClass, None, None, None, Precision_c_mf, Recall_c_mf, F1_c_mf,
                                              Precision_mf, Recall_mf, F1_mf, None, None, None, None, None,
                                              stdPrecision_mf, stdRecall_mf, stdF1_mf,None)
        resultSTR += "SL Score:  " + "%.3f"%SL + " +- %.3f" % stdDevSL + "\n"
        resultSTR += "EL Score:  " + "%.3f"%EL + " +- %.3f" % stdDevEL + "\n"

        resultSTR = "Multi-fold results:\n" + resultSTR
        resultSTR += "F1 per fold: \n"
        for i in range(len(F1_perfold)):
            resultSTR += "fold" + str(i) + "  :  " + "%.4f"%F1_perfold[i] + "\n"

        f = open(pathResult + self.name + str(self.alpha) + ".txt", "w+")
        f.write(resultSTR)
        f.close()
