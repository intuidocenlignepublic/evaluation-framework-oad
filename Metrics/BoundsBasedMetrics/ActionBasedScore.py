import os
from typing import List

import numpy as np

from Classes.Label import Label
from Classes.Sequence import Sequence
from Metrics.BoundsBasedMetrics.BoundsBasedMetric import BoundsBasedMetric


class ActionBasedScore(BoundsBasedMetric):
    def __init__(self, T_tolerance, nbClass, exportPerSequenceDetailResult):
        super().__init__("Action-Based score")
        self.T_tolerance = T_tolerance
        self.nbClass = nbClass
        self.exportPerSequenceDetailResult = exportPerSequenceDetailResult

    def Evaluate(self, sequence: List[Sequence], prediction: List[List[Label]], pathExport: str):
        assert len(sequence) == len(prediction)
        total_TP_per_class = np.zeros([self.nbClass + 1])
        total_FP_per_class = np.zeros([self.nbClass + 1])
        total_Count_per_class = np.zeros([self.nbClass + 1])
        if not os.path.exists(pathExport):
            os.makedirs(pathExport)
        for i in range(len(sequence)):
            TP_per_class, FP_per_class, Count_per_class = self.EvaluateSequence(sequence[i], prediction[i])

            if self.exportPerSequenceDetailResult:
                pathSeq = pathExport + "/Sequence/" + sequence[i].name + "/"
                if not os.path.exists(pathSeq):
                    os.makedirs(pathSeq)

                self.ExportPerformance(TP_per_class, FP_per_class, Count_per_class,
                                       pathSeq + self.name + str(self.T_tolerance) + ".txt")

            total_TP_per_class += TP_per_class
            total_FP_per_class += FP_per_class
            total_Count_per_class += Count_per_class

        self.ExportPerformance(total_TP_per_class, total_FP_per_class, total_Count_per_class,
                               pathExport + "/" + self.name + str(self.T_tolerance) + ".txt")

        return total_TP_per_class, total_FP_per_class, total_Count_per_class

    def bestMatching(self, gtLabel: List[Label], prediction: Label):
        closer = list(filter(lambda x: abs(prediction.startFrame - x.startFrame) >= 0,
                             sorted(gtLabel, key=lambda x: abs(prediction.startFrame - x.startFrame))))
        if len(closer) == 0:
            return None,None
        return closer[0], gtLabel.index(closer[0])

    def EvaluateSequence(self, sequence: Sequence, prediction: List[Label]):
        labels: List[Label] = list(sorted(sequence.labels, key=lambda x: x.startFrame))
        tags = np.zeros([len(labels)])
        TP = np.zeros([self.nbClass + 1])
        FP = np.zeros([self.nbClass + 1])
        nbPerclasses = np.zeros([self.nbClass + 1])

        for lab in labels:
            nbPerclasses[lab.classID] += 1

        for pred in prediction:
            closer,idCloser = self.bestMatching(labels, pred)
            if  closer is not None and tags[idCloser]==0 and closer.classID == pred.classID and (pred.startFrame - closer.startFrame) <= self.T_tolerance:
                tags[idCloser] = 1
                TP[closer.classID] += 1
            else:
                FP[pred.classID] += 1

        return TP, FP, nbPerclasses

    def ExportPerformance(self, TP_per_class, FP_per_class, Count_per_class, path):
        """
        Same use for individual sequences and for the whole test set
        :param TP_per_class:
        :param FP_per_class:
        :param Count_per_class:
        :param path: path to export the results
        :return:
        """

        Precision_c, Recall_c, F1_c, Precision, Recall, F1, TPAll, FPAll, CountAll = self.GetPrecisionRecallF1(
            TP_per_class, FP_per_class,
            Count_per_class)

        resultSTR = self.GetDescriptiveString(self.nbClass, TP_per_class, FP_per_class, Count_per_class, Precision_c,
                                              Recall_c, F1_c,
                                              Precision, Recall, F1, TPAll, FPAll, CountAll)

        with open(path, "w+") as f:
            f.write(resultSTR)
            f.close()



    def AggregateMultiFold(self, resultsFolds: List, pathResult: str, filesInFold):
        Precision_c_mf = []
        Recall_c_mf = []
        F1_c_mf = []
        Precision_mf = []
        Recall_mf = []
        F1_mf = []
        for TP_c, FP_c, Count_c in resultsFolds:
            Precision_c, Recall_c, F1_c, Precision, Recall, F1, _, _, _ = self.GetPrecisionRecallF1(TP_c, FP_c, Count_c)
            Precision_c_mf.append(Precision_c)
            Recall_c_mf.append(Recall_c)
            F1_c_mf.append(F1_c)
            Precision_mf.append(Precision)
            Recall_mf.append(Recall)
            F1_mf.append(F1)

        stdPrecision_mf = np.std(Precision_mf)
        stdRecall_mf = np.std(Recall_mf)
        stdF1_mf = np.std(F1_mf)

        Precision_c_mf = np.average(Precision_c_mf, axis=0)
        Recall_c_mf = np.average(Recall_c_mf, axis=0)
        F1_c_mf = np.average(F1_c_mf, axis=0)
        Precision_mf = np.average(Precision_mf, axis=0)
        Recall_mf = np.average(Recall_mf, axis=0)
        F1_mf = np.average(F1_mf, axis=0)

        resultSTR = self.GetDescriptiveString(self.nbClass, None, None, None, Precision_c_mf, Recall_c_mf, F1_c_mf,
                                              Precision_mf, Recall_mf, F1_mf, None, None, None,None,None,
                                              stdPrecision_mf, stdRecall_mf, stdF1_mf)

        f = open(pathResult + self.name + str(self.T_tolerance) + ".txt", "w+")
        f.write(resultSTR)
        f.close()
