import os
from typing import List, Tuple

from Classes.Label import Label
from Classes.Sequence import Sequence
from Metrics.BoundsBasedMetrics.BoundsBasedMetric import BoundsBasedMetric
import numpy as np
from matplotlib import pyplot as plt
from sklearn.metrics import ConfusionMatrixDisplay


class BoundedOnlineDetectionOverlap(BoundsBasedMetric):
    """
    Metric used in the paper
    Li, Y., Lan, C., Xing, J., Zeng, W., Yuan, C., Liu, J. (2016).
    Online Human Action Detection Using Joint Classification-Regression Recurrent Neural Networks.
    In: ECCV 2016.
    https://doi.org/10.1007/978-3-319-46478-7_13
    """

    def __init__(self, alpha, nbClass, canCorrect, exportPerSequenceDetailResult):
        """

        :param alpha: minimun overlap between the two bounds to be considered as a match (TP)
        """
        super().__init__("BoundedOnlineDetection")
        self.exportPerSequenceDetailResult = exportPerSequenceDetailResult
        self.nbClass = nbClass
        self.alpha = alpha
        self.canCorrect = canCorrect

    def Evaluate(self, sequence: List[Sequence], prediction: List[List[Label]], pathExport):
        """
        :param sequence: list of sequences
        :param prediction: list of list of labels
        :return:
        """
        assert len(sequence) == len(prediction)
        total_NTtoD_per_class_sum = np.zeros([self.nbClass + 1])
        total_TP_per_class = np.zeros([self.nbClass + 1])
        total_FP_per_class = np.zeros([self.nbClass + 1])
        total_Count_per_class = np.zeros([self.nbClass + 1])
        total_matconf = np.zeros([self.nbClass + 1, self.nbClass + 1])
        strCanCorrect = "canCorrect" if self.canCorrect else "noCorrectionAllowed"
        if not os.path.exists(pathExport):
            os.makedirs(pathExport)
        for i in range(len(sequence)):
            TP_per_class, FP_per_class, Count_per_class, Mat_conf, sum_NTtoD_seq_per_class = self.EvaluateSequence(
                sequence[i],
                prediction[i])
            # print(TP_per_class)
            NTtoD_avg = np.sum(sum_NTtoD_seq_per_class, axis=0) / sum(TP_per_class) if sum(
                TP_per_class) is not None and sum(TP_per_class) != 0 else None
            if self.exportPerSequenceDetailResult:
                pathSeq = pathExport + "/Sequence/" + sequence[i].name + "/"
                if not os.path.exists(pathSeq):
                    os.makedirs(pathSeq)

                self.ExportPerformance(TP_per_class, FP_per_class, Count_per_class, Mat_conf, sum_NTtoD_seq_per_class,
                                       NTtoD_avg,
                                       pathSeq + self.name + "_" + strCanCorrect + "_" + str(self.alpha) + ".txt")

            total_TP_per_class += TP_per_class
            total_FP_per_class += FP_per_class
            total_Count_per_class += Count_per_class
            total_matconf += Mat_conf
            total_NTtoD_per_class_sum += sum_NTtoD_seq_per_class

        NTtoD_per_class = np.divide(total_NTtoD_per_class_sum, total_TP_per_class,
                                    out=np.array([None] * len(total_TP_per_class)),
                                    where=total_TP_per_class != 0)
        if sum(total_TP_per_class) == 0:
            total_NTtoD_avg = None
        else:
            total_NTtoD_avg = np.sum(total_NTtoD_per_class_sum, axis=0) / sum(total_TP_per_class)
        self.ExportPerformance(total_TP_per_class, total_FP_per_class, total_Count_per_class, total_matconf,
                               NTtoD_per_class, total_NTtoD_avg,
                               pathExport + "/" + self.name + "_" + strCanCorrect + "_" + str(self.alpha) + ".txt")
        # precision_per_class = total_TP_per_class / (total_TP_per_class + total_FP_per_class)
        # recall_per_class = total_TP_per_class / (total_TP_per_class + total_Count_per_class)
        # F1_per_class = 2 * precision_per_class * recall_per_class / (precision_per_class + recall_per_class)
        return total_TP_per_class, total_FP_per_class, total_Count_per_class, NTtoD_per_class, total_NTtoD_avg

    def IntersectionOverUnion_deb(self, label1: Label, label2: Label):
        """
        the order doesn't matter
        :param label1: first label
        :param label2: second label
        :return: Intersection over Union, considering union from start and not from startGT
        we dont want to penalize the non-earliness on this criteria
        """
        start, end, startGT, endGT = label1.startFrame, label1.endFrame, label2.startFrame, label2.endFrame
        maxiStart = max(start, startGT)
        miniEnd = min(end, endGT)

        if miniEnd < maxiStart:  # no overlap
            return 0
        intersection = miniEnd - maxiStart + 1

        areaPred = (end - start) + 1
        assert max(startGT, start) <= endGT
        areaGTTroncated = (endGT - max(startGT, start)) + 1  # troncated with start of prediction

        union = areaPred + areaGTTroncated - intersection
        return intersection / union

    def EvaluateSequence(self, sequence: Sequence, prediction: List[Label]):
        """
        :param sequence: sequence, contain Class and Bounds
        :param prediction: list of labels [ClassID, startFrame, endFrame]
        :return: TP, FP, nbPerclasses, MatConf,
                NTtoD (sum of NTtoD per class), /!\ not divided by the number of TP
        """
        nbClass = self.nbClass
        flags = np.zeros([len(sequence.labels)])
        sequence.labels = sorted(sequence.labels, key=lambda x: x.startFrame)
        MatConf = np.zeros([nbClass + 1, nbClass + 1])  # +1: non-action,
        TP = np.zeros([nbClass + 1])
        FP = np.zeros([nbClass + 1])
        nbPerclasses = np.zeros([nbClass + 1])
        sum_NTtoD_per_class = np.zeros([nbClass + 1])
        # knowing the nb of examples per class allows to compute the recall
        for label in sequence.labels:
            classIDgt = label.classID
            nbPerclasses[classIDgt] += 1

        for labelPred in prediction:
            classId = labelPred.classID
            if classId == 0:
                print("Warning: classID = 0 found in the prediction, skipping")
                continue

            # get the GT which match the best the given prediction
            # /!\ can get the first of the list if the match is 0 for all
            gtIndex = int(np.argmax(list(
                map(lambda lab: self.IntersectionOverUnion_deb(labelPred, lab),
                    sequence.labels))))
            gtMax = np.max(list(
                map(lambda lab: self.IntersectionOverUnion_deb(labelPred, lab),
                    sequence.labels)))

            # print(gtIndex, gtMax)
            classIdGT = sequence.labels[gtIndex].classID
            if flags[gtIndex] == 0 and classId == classIdGT and gtMax > self.alpha:
                flags[gtIndex] = 1
                TP[classIdGT] += 1
                MatConf[classIdGT][classIdGT] += 1
                NTtoD_pred = min(max(0, labelPred.startFrame - sequence.labels[gtIndex].startFrame) / (
                        sequence.labels[gtIndex].endFrame - sequence.labels[gtIndex].startFrame + 1), 1)
                sum_NTtoD_per_class[classIdGT] += NTtoD_pred
            else:
                FP[classId] += 1
                if gtMax > 0 and not self.canCorrect:  # if there is an overlap with any GT bounds (took the first GT in the argmax if each is 0)
                    flags[gtIndex] = 1
                if classId != classIdGT and gtMax > 0:
                    MatConf[classIdGT][classId] += 1
                else:
                    MatConf[0][classId] += 1

        return TP, FP, nbPerclasses, MatConf, sum_NTtoD_per_class

    def ExportPerformance(self, TP_per_class, FP_per_class, Count_per_class, total_MatConf, NTtoD_per_class,
                          NTtoD_avg, path):

        """
        Same use for individual sequences and for the whole test set
        :param TP_per_class:
        :param FP_per_class:
        :param Count_per_class:
        :param path: path to export the results
        :return:
        """

        Precision_c, Recall_c, F1_c, Precision, Recall, F1, TPAll, FPAll, CountAll = self.GetPrecisionRecallF1(
            TP_per_class, FP_per_class,
            Count_per_class)

        self.resultsToSummarize["IoU_F1"] = (self.alpha, F1)

        # print(sum([total_MatConf[i][i] for i in range(self.nbClass+1)]),TPAll)
        # print(total_MatConf)
        assert sum([total_MatConf[i][i] for i in range(self.nbClass + 1)]) == TPAll
        try:
            # fig, ax = plt.subplot(figsize=(15, 15))
            disp = ConfusionMatrixDisplay(confusion_matrix=total_MatConf,
                                          display_labels=[i for i in
                                                          range(len(TP_per_class))])
            disp.plot(include_values=True, cmap=plt.cm.Blues, xticks_rotation="vertical", values_format=".1f")
            plt.savefig(path[:-4] + "_ConfMatrix.png")
            plt.close()
        except Exception as e:
            print(e)
            print("Confusion matrix not exported")

        resultSTR = self.GetDescriptiveString(self.nbClass, TP_per_class, FP_per_class, Count_per_class,
                                              Precision_c,
                                              Recall_c, F1_c,
                                              Precision, Recall, F1, TPAll, FPAll, CountAll, NTtoD_per_class,
                                              NTtoD_avg)

        with open(path, "w+") as f:
            f.write(resultSTR)
            f.close()

    def AggregateMultiFold(self, resultsFolds: List, pathResult: str, filesInFold):
        """
        Aggregate the results of the multi-fold cross-validation using macro-averaging
        :param filesInFold:
        :param resultsFolds: the TP,FP and count for each fold
        :return:
        """
        Precision_c_mf = []
        Recall_c_mf = []
        F1_c_mf = []
        Precision_mf = []
        Recall_mf = []
        F1_mf = []
        NTtoD_c_mf = []
        total_NTtoD_avgs = []
        for TP_c, FP_c, Count_c, NTtoD_c, total_NTtoD_avg in resultsFolds:
            Precision_c, Recall_c, F1_c, Precision, Recall, F1, _, _, _ = self.GetPrecisionRecallF1(TP_c, FP_c,
                                                                                                    Count_c)
            Precision_c_mf.append(Precision_c)
            Recall_c_mf.append(Recall_c)
            F1_c_mf.append(F1_c)
            Precision_mf.append(Precision)
            Recall_mf.append(Recall)
            F1_mf.append(F1)
            NTtoD_c_mf.append(NTtoD_c)
            total_NTtoD_avgs.append(total_NTtoD_avg)

        stdF1_mf = np.std(F1_mf)
        stdPrecision_mf = np.std(Precision_mf)
        stdRecall_mf = np.std(Recall_mf)
        stdNTtoD_c_mf = np.std([x for x in total_NTtoD_avgs if x is not None])


        Precision_c_mf = np.average(Precision_c_mf, axis=0)
        Recall_c_mf = np.average(Recall_c_mf, axis=0)
        F1_c_mf = np.average(F1_c_mf, axis=0)
        Precision_mf = np.average(Precision_mf, axis=0)
        Recall_mf = np.average(Recall_mf, axis=0)
        F1_perfold = F1_mf
        F1_mf = np.average(F1_mf, axis=0)
        # average NTtoD where its not None
        total_NTtoD_avgs = np.average([x for x in total_NTtoD_avgs if x is not None], axis=0)
        # print("NTOTOD", NTtoD_c_mf)
        NTtoD_c_avg_mf = []
        for i in range(len(NTtoD_c_mf[0])):
            nttod = [NTtoD_c_mf[j][i] for j in range(len(NTtoD_c_mf)) if NTtoD_c_mf[j][i] is not None]

            if len(nttod) > 0:
                avg = np.average(nttod)
            else:
                avg = None
            NTtoD_c_avg_mf.append(avg)

        # compute the average is ignore None values

        resultSTR = self.GetDescriptiveString(self.nbClass, None, None, None, Precision_c_mf, Recall_c_mf, F1_c_mf,
                                              Precision_mf, Recall_mf, F1_mf, None, None, None, NTtoD_c_avg_mf,
                                              total_NTtoD_avgs, stdPrecision_mf, stdRecall_mf, stdF1_mf, stdNTtoD_c_mf)
        strCanCorrect = "canCorrect" if self.canCorrect else "noCorrectionAllowed"
        resultSTR = "Multi-fold results:\n" + resultSTR
        resultSTR += "F1 per fold: \n"
        for i in range(len(F1_perfold)):
            resultSTR += "fold" + str(i) + "  :  " + "%.4f"%F1_perfold[i] + "\n"

        f = open(pathResult + self.name + "_" + strCanCorrect + "_" + str(self.alpha) + ".txt", "w+")
        f.write(resultSTR)
        f.close()
