import os.path
from typing import List

import PIL
import matplotlib
import matplotlib.pyplot as plt

from Classes.Label import Label


def StrategyForPerFrameToBounds(predictCl)-> List[Label]:
    """
    Simple strategy to simplify the draw of perframe predictions.
    :param predictCl: predicted class for each frame
    :param PredictSt: predicted start for each frame
    :param PredictEnd: predicted end for each frame
    :return:
    """
    bounds = []
    currentPrediction = 0
    startPredict = 0
    for i in range(len(predictCl)):
        pred = predictCl[i]
        # if pred == 0:
        #     continue

        if pred != currentPrediction:
            if currentPrediction != 0:
                bounds.append(Label(currentPrediction, startPredict, i - 1))
            currentPrediction = pred
            startPredict = i
    return bounds

def drawSequence(path, name, GTStartEnd:List[Label],predictionBound:List[Label],labelsPredPerFrame,nbClass):
    """
    draw the sequence with the ground truth and the prediction, produce a png file saved in path
    :param name: str
    :param GTStartEnd: List[Label]
    :param predictionBound: List[Label]
    :param labelsPredPerFrame: List[ActionID]
    :param seqLen: int, len of the sequence
    :param nbClass: int
    :return:
    """
    predictionBoundWithoutBlank = None
    if labelsPredPerFrame is not None:
        predictionBoundWithoutBlank  = StrategyForPerFrameToBounds(labelsPredPerFrame)

    cmap = plt.cm.get_cmap("gist_rainbow", nbClass+1)  # hsv
    # cols = 5
    # figsize = (cols, 20)
    # rows = len(imagesNames) // cols + 1
    if not os.path.exists(path):
        os.makedirs(path)

    step = 1
    nbElem = max([max(map(lambda x:x.endFrame,GTStartEnd)),max(map(lambda x:x.endFrame,predictionBound),default=0)])
    nbRows = 3*(3 if predictionBoundWithoutBlank is not None else 2)+5
    gs = matplotlib.gridspec.GridSpec(nbRows, nbElem//step+1)
    # ax1 = plt.subplot(gs[:3, :3])
    # ax2 = plt.subplot(gs[0, 3])
    # ax3 = plt.subplot(gs[1, 3])
    # ax4 = plt.subplot(gs[2, 3])
    # ax1.plot(series[0])
    # ax2.plot(series[1])
    # ax3.plot(series[2])
    # ax4.plot(series[3])
    fig = plt.figure(figsize=(nbElem//step+1,nbRows),dpi=2)
    # plt.tight_layout()
    axs = []
    # plt.suptitle("GT "+str(GTStartEnd[:,0])[1:-1],fontsize=45//3)
    id=0

    unitWidth = 1/((nbElem+1)/(step))
    # for ax, imName in zip(axs, imagesNames):
    # for i in range(0,seqLen,step):
    #     # imName= imagesNames[i]
    #     # img = mpimg.imread(pathImgForThisSequence+separator+imName)
    #     # ax = fig.add_subplot(rows, cols, i+1)
    #     # ax = plt.subplot(gs[:2,i//step])
    #     # ax.set_title("Class " +str(GT[id])+" f "+str(id),fontsize=7)
    #     # imag = ax.imshow(img)
    #     # accSTR = 'Accepted' if rejection[id] else "Rejected"
    #     # color = 'green' if rejection[id] else "red"
    #     # colorPred = 'green' if prediction[id]==GT[id] else "red"
    #     # plt.yticks([],rotation=90)
    #     plt.xticks([])
    #     # ax.set_ylabel("pred:"+str(prediction[id]), fontsize=10,color=colorPred)
    #     # ax.set_xlabel(accSTR, fontsize=7,color=color)
    #     startDraw = i*unitWidth
    #
    #     axPrediBrut.add_artist(plt.Rectangle((startDraw, 0,), unitWidth, 2, facecolor=cmap(prediction[id])))
    #
    #     id+=step
    axGT = plt.subplot(gs[0:4,:])

    for gt_start_end in GTStartEnd:
        gtId,start,end = gt_start_end.classID,gt_start_end.startFrame,gt_start_end.endFrame
        startSc = ((start)//step)*unitWidth
        rect = plt.Rectangle((startSc, 0), (end//step-start//step+1)*unitWidth, 2, facecolor=cmap(gtId))
        axGT.add_artist(rect)

        actPoint = gt_start_end.actionPoint
        if actPoint is not None:
            startSc = ((actPoint) // step) * unitWidth
            rect = plt.Rectangle((startSc, 0.1), unitWidth*1, 0.3, facecolor="white")
            axGT.add_artist(rect)
            startSc = ((actPoint) // step) * unitWidth
            rect = plt.Rectangle((startSc, 0.4), unitWidth*1, 0.25, facecolor="red")
            axGT.add_artist(rect)
            startSc = ((actPoint) // step) * unitWidth
            rect = plt.Rectangle((startSc, 0.65), unitWidth*1, 0.25, facecolor="black")
            axGT.add_artist(rect)
        # print("ax,",ax)

    axPred = plt.subplot(gs[4:11,:])

    for gt_start_end in predictionBound:
        gtId,start,end = gt_start_end.classID,gt_start_end.startFrame,gt_start_end.endFrame
        startSc = ((start)//step)*unitWidth
        rect = plt.Rectangle((startSc, 0), ((end)//step-start//step+1)*unitWidth, 2, facecolor=cmap(gtId))
        axPred.add_artist(rect)

    axs.append(axPred)


    if predictionBoundWithoutBlank is not None :
        axPredScnd = plt.subplot(gs[11:14, :])
        for gt_start_end in predictionBoundWithoutBlank:
            gtId,start,end = gt_start_end.classID,gt_start_end.startFrame,gt_start_end.endFrame
            startSc = ((start)//step)*unitWidth
            rect = plt.Rectangle((startSc, 0), ((end)//step-start//step+1)*unitWidth, 2, facecolor=cmap(gtId))
            axPredScnd.add_artist(rect)
        axs.append(axPredScnd)
    axs.append(axGT)
    #
    # axPred = plt.subplot(gs[2,:])
    # axPred.plot([0.5,0.5],"k")
    # axPred.set_xlim(0,1)
    # axPred.set_ylim(0.0,1)


        # print("ax,",ax)
    for ax in axs:
        ax.set_xticks([])
        ax.set_yticks([])
        ax.set_xticklabels([])
        ax.set_yticklabels([])
        # ax.set_aspect('equal')
    plt.subplots_adjust(wspace=0, hspace=0)
    # plt.savefig(str(name)+".svg")
    # plt.savefig(str(name)+".png")
    plt.savefig(path+name+".png")
    plt.close()
