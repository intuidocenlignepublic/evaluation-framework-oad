from typing import List


class Label:
    """
    Class representing a label, with a classID, a start frame, an end frame and an action point (if any)
    """
    def __init__(self,classID,startFrame,endFrame,actionPoint=None):
        """

        :param classID: between 1 and nbClasses
        :param startFrame: start frame of the label
        :param endFrame: end frame of the label
        :param actionPoint: action point of the label (optional)
        """
        self.classID = classID
        self.startFrame = startFrame
        self.endFrame = endFrame
        self.actionPoint = actionPoint

    @classmethod
    def LabelsFromFile(cls, pathFile)->List["Label"]:
        """
        Load labels from a file
        :param pathFile: the path of the file
        :return:
        """
        labels = []
        with open(pathFile, 'r') as f:
            for line in f:
                line = line.strip()
                if line == '':
                    continue
                line = line.split(',')
                classID = int(line[0])
                startFrame = int(line[1])
                endFrame = int(line[2])
                if len(line) >= 4:
                    actionPoint = int(line[3])
                else:
                    actionPoint = None
                labels.append(Label(classID,startFrame,endFrame,actionPoint))
        return labels

    def __str__(self):
        return str(self.classID) + " " + str(self.startFrame) + " " + str(self.endFrame) + " " + str(self.actionPoint)