from typing import List

import numpy as np

from Classes.Label import Label


class Sequence:
    """
    A sequence is a list of labels.
    Note that the data values are never used in this project, only the labels are used.
    So the lastFrame is computed as the max of the endFrame of the labels.
    Metrics should handle by themselves when a predicted bound is over the lastFrame.
    """
    def __init__(self, sequenceName, labels:List[Label]):
        self.name = sequenceName
        self.labels = labels
        self.lastFrame = max(map(lambda x:x.endFrame,labels))

    def getPerFrameLabel(self):
        perFrameLabel = np.zeros([self.lastFrame+1])
        for label in self.labels:
            perFrameLabel[label.startFrame:label.endFrame+1] = label.classID
        return perFrameLabel

    def __str__(self):
        return self.name + " " + str(self.labels)

